//
//  SongSketch.swift
//
//  Created by Jonathan Ortheden on 09/06/2016
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

fileprivate func < <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

public enum RecordingType: Int {
  case Bounce = 0
  case Instrumental = 1
  case Stem = 2
  case TVTrack = 3
  
  var presentableHeadingTitle: String {
    switch self {
    case.Bounce: return "BOUNCES"
    case.Instrumental: return "INSTRUMENTALS & TV TRACKS"
    case.Stem: return "STEMS"
    case.TVTrack: return "INSTRUMENTALS & TV TRACKS"
    }
  }
  
}

open class SongSketch: NSObject, NSCoding {
  
  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kOwnerKey = "owner"
  private let kTitleKey = "title"
  private let kInternalIdentifierKey = "id"
  private let kResourceKey = "resource"
  private let kCommentsCountKey = "comments_count"
  private let kCommentsKey = "comments"
  private let kDescriptionValueKey = "description"
  private let kBookmarksKey = "bookmarks"
  private let kCreatedKey = "created"
  private let kStatusKey = "status"
  private let kUpdatedKey = "updated"
  private let kPrivateKey = "isPrivate"
  private let kOfficialKey = "official_version"
  private let kSongKey = "song"
  private let kRecordingTypeKey = "version_type_id"
  
  // MARK: Properties
  open var title: String?
  open var internalIdentifier: Int?
  open var songIdentifier: Int?
  open var resource: Resource?
  open var commentsCount: Int?
  //open var comments: [Comments]?
  open var descriptionValue: String?
  //open var bookmarks: [Bookmarks]?
  open var created: Int64?
  open var status: Int?
  open var updated: Int64?
  open var isPrivate: Bool?
  open var officialVersion: Bool?
  open var recordingType: RecordingType?
  open var duration: TimeInterval?
  
  // MARK: SwiftyJSON Initalizers
  /**
   Initates the class based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
   */
  convenience public init(object: AnyObject) {
    self.init(json: JSON(object))
  }
  
  public init(href: String) {
    self.resource = Resource(href: href)
  }
  
  /**
   Initates the class based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
   */
  public init(json: JSON) {
    title = json[kTitleKey].string
    internalIdentifier = json[kInternalIdentifierKey].int
    songIdentifier = json[kSongKey].int
    resource = Resource(json: json[kResourceKey])
    commentsCount = json[kCommentsCountKey].int
    isPrivate = json[kPrivateKey].bool
    officialVersion = json[kOfficialKey].bool
    descriptionValue = json[kDescriptionValueKey].string
    
    /*
    comments = []
    if let items = json[kCommentsKey].array {
      for item in items {
      }
    } else {
      comments = nil
    }
    
    bookmarks = []
    if let items = json[kBookmarksKey].array {
      for item in items {
        //bookmarks?.append(Bookmarks(json: item))
      }
    } else {
      bookmarks = nil
    }
 */
    created = json[kCreatedKey].int64
    status = json[kStatusKey].int
    updated = json[kUpdatedKey].int64
    
    if let type = json["version_type_id"].int {
      recordingType = RecordingType(rawValue: type)
    }
    
    if let duration = json["duration"].int {
      self.duration = TimeInterval(duration)
    }
    
  }
  
  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
   */
  open func dictionaryRepresentation() -> [String : Any ] {
    
    var dictionary: [String : AnyObject ] = [ : ]
    if title != nil {
      dictionary.updateValue(title! as AnyObject, forKey: kTitleKey)
    }
    if internalIdentifier != nil {
      dictionary.updateValue(internalIdentifier! as AnyObject, forKey: kInternalIdentifierKey)
    }
    if resource != nil {
      dictionary.updateValue(resource!.dictionaryRepresentation() as AnyObject, forKey: kResourceKey)
    }
    if commentsCount != nil {
      dictionary.updateValue(commentsCount! as AnyObject, forKey: kCommentsCountKey)
    }
    if descriptionValue != nil {
      dictionary.updateValue(descriptionValue! as AnyObject, forKey: kDescriptionValueKey)
    }
  
    if created != nil {
      dictionary.updateValue(created! as AnyObject, forKey: kCreatedKey)
    }
    if status != nil {
      dictionary.updateValue(status! as AnyObject, forKey: kStatusKey)
    }
    if updated != nil {
      dictionary.updateValue(updated! as AnyObject, forKey: kUpdatedKey)
    }
    
    if songIdentifier != nil {
      dictionary.updateValue(songIdentifier! as AnyObject, forKey: kSongKey)
    }
    
    if officialVersion != nil {
      dictionary.updateValue(officialVersion! as AnyObject, forKey: kOfficialKey)
    }
    
    if isPrivate != nil {
      dictionary.updateValue(isPrivate! as AnyObject, forKey: kPrivateKey)
    }
    
    if recordingType != nil {
      dictionary.updateValue(recordingType!.rawValue as AnyObject, forKey: kRecordingTypeKey)
    }
    
    return dictionary
  }
  
  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    title = aDecoder.decodeObject(forKey: kTitleKey) as? String
    internalIdentifier = aDecoder.decodeObject(forKey: kInternalIdentifierKey) as? Int
    resource = aDecoder.decodeObject(forKey: kResourceKey) as? Resource
    commentsCount = aDecoder.decodeObject(forKey: kCommentsCountKey) as? Int
    descriptionValue = aDecoder.decodeObject(forKey: kDescriptionValueKey) as? String
    //bookmarks = aDecoder.decodeObject(forKey: kBookmarksKey) as? [Bookmarks]
    created = aDecoder.decodeObject(forKey: kCreatedKey) as? Int64
    status = aDecoder.decodeObject(forKey: kStatusKey) as? Int
    updated = aDecoder.decodeObject(forKey: kUpdatedKey) as? Int64
    isPrivate = aDecoder.decodeObject(forKey: kPrivateKey) as? Bool
    officialVersion = aDecoder.decodeObject(forKey: kOfficialKey) as? Bool
    songIdentifier = aDecoder.decodeObject(forKey: kSongKey) as? Int
    
    if let type = aDecoder.decodeObject(forKey: kRecordingTypeKey) as? Int {
      recordingType = RecordingType(rawValue: type)
    }
  }
  
  open func encode(with aCoder: NSCoder) {
    aCoder.encode(officialVersion, forKey: kOfficialKey)
    aCoder.encode(title, forKey: kTitleKey)
    aCoder.encode(internalIdentifier, forKey: kInternalIdentifierKey)
    aCoder.encode(resource, forKey: kResourceKey)
    aCoder.encode(commentsCount, forKey: kCommentsCountKey)
    aCoder.encode(descriptionValue, forKey: kDescriptionValueKey)
    //aCoder.encode(bookmarks, forKey: kBookmarksKey)
    aCoder.encode(created, forKey: kCreatedKey)
    aCoder.encode(status, forKey: kStatusKey)
    aCoder.encode(updated, forKey: kUpdatedKey)
    aCoder.encode(isPrivate, forKey: kPrivateKey)
    aCoder.encode(songIdentifier, forKey: kSongKey)
    aCoder.encode(recordingType?.rawValue, forKey: kRecordingTypeKey)
  }
}

