//  AudioProcessor.swift
//
//  Created by Maz Naiini on 2017-07-07.
//  Copyright © 2017 Auddly. All rights reserved.

import Foundation
import AVFoundation
import Accelerate
import Alamofire

class AudioProcessor {
  
  private var reader: AVAssetReader?
  private var output: AVAssetReaderTrackOutput?
  
  let queue = DispatchQueue(label: "Audio Proccess Queue")
  
  func waveformSamples(from asset: NowPlayingAsset, intervalSize: Int = 30 ,updateWith: @escaping ([Float]?, Bool) -> Void) {
    
    switch asset {
    case .local(let localAsset):
      
      let localAsset = AVURLAsset(url: localAsset.url)

      queue.async {[weak self] in
        
        let numberOfintervals = Int(floor (localAsset.duration.seconds / Double(intervalSize)))
        
        for intervalCounter in 0...numberOfintervals {
          guard self != nil else {return}
          let start = CMTime(seconds: Double(intervalCounter * intervalSize), preferredTimescale: 1)
          let duration = CMTime(seconds: Double(intervalSize), preferredTimescale: 1)
          updateWith(self!.samples(for: localAsset, start: start, duration: duration), false)
          print(intervalCounter)
        }
        
        DispatchQueue.main.async {[weak self] in
          guard self != nil else {return}
          updateWith(self!.reader?.status == .failed ?  nil:[], true)
        }
        
      }
    case .remote(let remoteAsset):
      guard let assetWaveformUrl = remoteAsset.audioSketch.resource?.href else { return }
      
      Alamofire.request("http://villastrompis.se/data.dat").responseData { (dataResponse) in
        if let data = dataResponse.data {
          let dataPoints = self.getPointsFromDatFile(data: data)
          updateWith(dataPoints, false)
        } else {
          updateWith(self.reader?.status == .failed ?  nil:[], true)
        }
      }
    }
  }
  
  func getPointsFromDatFile(data: Data) -> [Float] {
    
    var points = [Float]()
    let data = NSData(data: data)
    let length = (getBigIntFromData(data: data, offset: 16) * 2) - 1 // 2 channels and loop starting at 0
    
    for i in 0 ... length {
      let result = getDataPointFromData(data: data, offset: 20 + (i * 2))
      points.append(Float(result))
    }
    
    return points
  }
  
  private func getDataPointFromData(data: NSData, offset: Int) -> Int {
    let rng = NSRange(location: offset, length: 2)
    var i = [Int16](repeating:0, count: 1)
    
    data.getBytes(&i, range: rng)
    return Int(i[0])// return Int(i[0]) for littleEndian
  }
  
  private func getBigIntFromData(data: NSData, offset: Int) -> Int {
    let rng = NSRange(location: offset, length: 4)
    var i = [UInt32](repeating:0, count: 1)
    
    data.getBytes(&i, range: rng)
    return Int(i[0])// return Int(i[0]) for littleEndian
  }
  
  private func samples(for asset: AVURLAsset, start: CMTime? = nil, duration: CMTime? = nil) -> [Float] {
    
    do { reader = try AVAssetReader(asset: asset) } catch { return []}
    guard let track = reader?.asset.tracks.first else {return []}
    
    output = AVAssetReaderTrackOutput(track: track, outputSettings: settings)
    reader?.add(output!)
    
    if let start = start, let duration = duration {
      reader?.timeRange = CMTimeRangeMake(start, duration)
    }
    getSamples(from: reader!)
    return downSample(samples)
  }
  
  private var samples = [Float]()
  
  private func getSamples(from reader: AVAssetReader) {
    
    samples = []
    
    reader.startReading()
    while reader.status == .reading {
      
      let output = reader.outputs.first!
      
      guard let sampleBuffer = output.copyNextSampleBuffer(),
        let blockBuffer = CMSampleBufferGetDataBuffer(sampleBuffer) else {continue}
      
      let blockBufferLength = CMBlockBufferGetDataLength(blockBuffer)
      let sampleLength = CMSampleBufferGetNumSamples(sampleBuffer) * channelCount
      
      var data = Data(capacity: blockBufferLength)
      data.withUnsafeMutableBytes { (blockSamples: UnsafeMutablePointer<Int16>) in
        CMBlockBufferCopyDataBytes(blockBuffer, 0, blockBufferLength, blockSamples)
        CMSampleBufferInvalidate(sampleBuffer)
        samples.append(contentsOf: floatArray(from: blockSamples, ofLength: sampleLength))
      }
      
    }
    
  }
  
  private func floatArray(from samplesPointer: UnsafeMutablePointer<Int16>, ofLength length: Int) -> [Float]{
    let samplesToProcess = vDSP_Length(length)
    var samples = [Float](repeating: 0.0, count: Int(samplesToProcess))
    vDSP_vflt16(samplesPointer, 1, &samples, 1, samplesToProcess)
    return samples
  }
  
}

extension AudioProcessor {
  
  fileprivate func downSample(_ samples: [Float]) -> [Float] {
    var processingBuffer = [Float](repeating: 0.0, count: Int(samples.count))
    let sampleCount = vDSP_Length(samples.count)
    vDSP_vabs(samples, 1, &processingBuffer, 1, sampleCount);
    
    let samplesPerPixel = 630
    let filter = [Float](repeating: 1.0 / Float(samplesPerPixel), count: Int(samplesPerPixel))
    let downSampledLength = Int(samples.count / samplesPerPixel)
    var downSampledData = [Float](repeating:0.0, count:downSampledLength)
    vDSP_desamp(processingBuffer, vDSP_Stride(samplesPerPixel), filter, &downSampledData, vDSP_Length(downSampledLength), vDSP_Length(samplesPerPixel))
    return downSampledData
  }

}

private extension AudioProcessor {
  
  var settings: [String: Any] {
    return [
      AVFormatIDKey: kAudioFormatLinearPCM,
      AVLinearPCMBitDepthKey: 16,
      AVLinearPCMIsBigEndianKey: false,
      AVLinearPCMIsFloatKey: false,
      AVLinearPCMIsNonInterleaved: false
    ]
  }
  
  var channelCount: Int {return 1}
  
}
