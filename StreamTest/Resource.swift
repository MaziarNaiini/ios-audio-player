//
//  Resource.swift
//
//  Created by Jonathan Ortheden on 17/02/2016
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

open class Resource: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	let kResourceInternalIdentifierKey = "id"
	let kResourceHrefKey = "href"

    // MARK: Properties
	open var internalIdentifier: Int?
	open var href: String?

    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
  
    public init(href: String) {
      self.href = href
    }
  
    public init(json: JSON) {
		internalIdentifier = json[kResourceInternalIdentifierKey].int
		href = json[kResourceHrefKey].string

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    open func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if internalIdentifier != nil {
			dictionary.updateValue(internalIdentifier! as AnyObject, forKey: kResourceInternalIdentifierKey)
		}
		if href != nil {
			dictionary.updateValue(href! as AnyObject, forKey: kResourceHrefKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		internalIdentifier = aDecoder.decodeObject(forKey: kResourceInternalIdentifierKey) as? Int
		href = aDecoder.decodeObject(forKey: kResourceHrefKey) as? String

    }

    open func encode(with aCoder: NSCoder) {
		aCoder.encode(internalIdentifier, forKey: kResourceInternalIdentifierKey)
		aCoder.encode(href, forKey: kResourceHrefKey)

    }

}
