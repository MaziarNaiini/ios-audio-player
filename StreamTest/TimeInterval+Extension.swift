//
//  Double+Extension.swift
//  Auddly
//
//  Created by Maz Naiini on 2017-07-11.
//  Copyright © 2017 Auddly. All rights reserved.
//

import Foundation

extension TimeInterval {
  
  var humanReadbleString: String {
    let formatter = DateComponentsFormatter()
    formatter.zeroFormattingBehavior = .pad
    formatter.allowedUnits = [.hour, .minute, .second]
    return formatter.string(from: self) ?? ""
  }
  
}
