//
//  Color.swift
//  Auddly
//
//  Created by Jonathan Ortheden on 1/19/17.
//  Copyright © 2017 Auddly. All rights reserved.
//

import UIKit

extension UIColor {
  
  // MARK: - COLORS

  static let brand      = UIColor(hexString: "#ad49f5")
  static let brandLight = UIColor(hexString: "#bd61ff")
  static let brandDark  = UIColor(hexString: "#9e34eb")
  
  static let type     = UIColor(hexString: "#1d2238")
  static let typeSub  = UIColor(hexString: "#6a75ab")
  static let typeDark = UIColor(hexString: "#fff")
  
  static let ico    = UIColor(hexString: "#6a75ab")
  static let icoSub = UIColor(hexString: "#d8dae6")
  
  static let border    = UIColor(hexString: "#d8dae6")
  static let borderSub = UIColor(hexString: "#ebedfa")
  
  static let bg         = UIColor(hexString: "#fff")
  static let bgDark     = UIColor(hexString: "#1d2238")
  static let bgSub      = UIColor(hexString: "#f5f6fa")
  static let bgSelected = UIColor(hexString: "#e5eaff")
  
  static let logo         = UIColor(hexString: "#f4cb4a")
  
  static let defaultC     = UIColor(hexString: "#b8bbcc")
  static let defaultLight = UIColor(hexString: "#d2d3d6")
  static let defaultDark  = UIColor(hexString: "#9fa5c2")
  
  static let info      = UIColor(hexString: "#627af5")
  static let infoBg    = UIColor(hexString: "#e5eaff")
  static let infoLight = UIColor(hexString: "#7a91ff")
  static let infoDark  = UIColor(hexString: "#4b66eb")
  
  static let link       = UIColor(hexString: "#4b66eb")
  static let linkActive = UIColor(hexString: "#627af5")
  static let linkHover  = UIColor(hexString: "#7a91ff")

  static let success      = UIColor(hexString: "#25d26b")
  static let successLight = UIColor(hexString: "#39db7a")
  static let successDark  = UIColor(hexString: "#11ad50")
  static let successBg    = UIColor(hexString: "#e0ffed")
  
  static let warning      = UIColor(hexString: "#ed4c4c")
  static let warningLight = UIColor(hexString: "#f76363")
  static let warningDark  = UIColor(hexString: "#e33636")
  static let warningBg    = UIColor(hexString: "#ffebeb")
  
  static let transparent  = UIColor.clear
  
  static let creation  = UIColor(hexString: "#abc8f5")
  static let pitchable = UIColor(hexString: "#627af5")
  static let hold      = UIColor(hexString: "#ad49f5")
  static let released  = UIColor(hexString: "#25d26b")
  
  static let avatar1Type = UIColor(hexString: "#59628f")
  static let avatar1Bg   = UIColor(hexString: "#c9d0f5")
  static let avatar2Type = UIColor(hexString: "#8f5980")
  static let avatar2Bg   = UIColor(hexString: "#f5c9e9")
  static let avatar3Type = UIColor(hexString: "#598f6d")
  static let avatar3Bg   = UIColor(hexString: "#c9f5da")
  static let avatar4Type = UIColor(hexString: "#8f5959")
  static let avatar4Bg   = UIColor(hexString: "#f5c9c9")
  static let avatar5Type = UIColor(hexString: "#8f7559")
  static let avatar5Bg   = UIColor(hexString: "#f5e0c9")
  
  static func custom(hexString: String, alpha: Double) -> UIColor {
    return UIColor(hexString: hexString).withAlphaComponent(CGFloat(alpha))
  }
  
}
