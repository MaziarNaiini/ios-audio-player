//
//  Asset.swift
//  StreamTest
//
//  Created by Jonathan Ortheden on 2017-08-22.
//  Copyright © 2017 Secure Apps. All rights reserved.
//

import Foundation

struct LocalAudioAsset {
  let url: URL
  let title: String
  let subtitle: String
}

struct RemoteAudioAsset {
  let audioSketch: SongSketch
  let title: String
  let subtitle: String
}

enum NowPlayingAsset {
  case local(LocalAudioAsset)
  case remote(RemoteAudioAsset)
}
