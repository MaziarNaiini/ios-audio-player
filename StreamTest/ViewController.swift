//
//  ViewController.swift
//  StreamTest
//
//  Created by Jonathan Ortheden on 7/13/17.
//  Copyright © 2017 Secure Apps. All rights reserved.
//

//"http://www.siop.org/conferences/09con/09_siop_005.mp3"

import UIKit
import AVFoundation
import Alamofire

extension AVPlayer {
  var isPlaying: Bool {
    return rate != 0 && error == nil
  }
}

struct Bookmark {
  var time: TimeInterval!
}

func ==(lhs: Bookmark, rhs: Bookmark) -> Bool {
  return lhs.time == rhs.time
}

class ViewController: UIViewController {
  
  enum State {
    case downloading
    case paused
    case playing
    case none
  }
  
  @IBOutlet weak var playBtn: UIButton!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet var timeLabel: UILabel!
  @IBOutlet var totalTimeLabel: UILabel!
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var subtitleLabel: UILabel!
  @IBOutlet var bookmarkButton: UIButton!
  
  var audioPlayer: AVPlayer? = nil
  var audioTimeObserver: Any?
  var shouldObserveAudioPlayer = true
  var requiresPlayAfterUserInteraction = false
  var bookmarkWidth: CGFloat = 40.0
  
  
  var indicatorWidth: CGFloat = 1.0
  var bookmarkLineWidth: CGFloat = 1.0
  var timelineViewHeight: CGFloat = 20
  var positionIndicatorTopOverlap: CGFloat = 6 // how much of the position indicator be overlapping the timeline view above
  var bookmarkViewTooltipHeight: CGFloat = 16

  var bookmarks = [Bookmark]() {
    didSet {
      self.collectionView.reloadData()
    }
  }
  
  var bookmarkViews = [BookmarkView]()
  
  var selectedBookmark: Bookmark?
  
  var waveformPoints = [Float]() {
    didSet {
      DispatchQueue.main.async {[weak self] in
        self?.collectionView.reloadData()
      }
    }
  }
  
  var state = State.none {
    didSet {
      switch state {
      case .playing: audioPlayer?.play()
      case.paused: audioPlayer?.pause()
      default: break
      }
      updateButtons()
    }
  }
  
  
  var asset: NowPlayingAsset! {
    didSet { set(asset: asset) }
  }
  
  var processor = AudioProcessor()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    let insetWidth = collectionView.frame.width/2
    collectionView.contentInset = UIEdgeInsets(top: 0, left: insetWidth, bottom: 0, right: insetWidth)
    
    titleLabel.text = nil
    subtitleLabel.text = nil
  }
  
  @IBAction func setLocalAsset(_ sender: Any) {
    let localUrl = Bundle.main.url(forResource: "long", withExtension: "mp3")!
    let localAsset = LocalAudioAsset(url: localUrl, title: "Local", subtitle: "Project")
    asset = .local(localAsset)
  }
  @IBAction func setRemoteAsset(_ sender: Any) {
    let audioSketch = SongSketch(href: "http://www.siop.org/conferences/09con/09_siop_005.mp3")
    let remoteAsset = RemoteAudioAsset(audioSketch: audioSketch, title: "Remote", subtitle: "Project")
    asset = .remote(remoteAsset)
  }
  
  private func set(asset: NowPlayingAsset) {
    
    if let observer = audioTimeObserver {
      audioPlayer?.removeTimeObserver(observer)
    }
    
    do {
      let session = AVAudioSession.sharedInstance()
      try session.setCategory(AVAudioSessionCategoryPlayback, with: [.defaultToSpeaker,.allowBluetooth])
      try session.setActive(true)
    } catch let error as NSError {
      NSLog("Could not set session: \(error)")
    }
    
    var assetUrl: URL
    
    switch asset {
    case .local(let localFile):
      titleLabel.text = localFile.title
      subtitleLabel.text = localFile.subtitle
      
      assetUrl = localFile.url
    case .remote(let remoteFile):
      guard let assetHref = remoteFile.audioSketch.resource?.href, let url = URL(string: assetHref) else { return }
      assetUrl = url
      
      titleLabel.text = remoteFile.title
      subtitleLabel.text = remoteFile.subtitle
    }
    
    let urlAsset = AVURLAsset(url: assetUrl)
    
    let playerItem = AVPlayerItem(asset: urlAsset)
    
    audioPlayer = AVPlayer(playerItem:playerItem)
    audioPlayer?.rate = 1.0;
    audioPlayer?.playImmediately(atRate: 1.0)
    
    audioTimeObserver = audioPlayer?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 70), queue: nil, using: {[weak self] (time: CMTime) in
      self?.update(with: time)
    })
    
    waveformPoints.removeAll()
    collectionView.bounces = false
    
    processor.waveformSamples(from: asset) {[weak self] (samples: [Float]?, completed: Bool) in
      guard let samples = samples else {return}
      //print(samples.count)
      self?.waveformPoints.append(contentsOf: samples)
      guard completed else {return}
      self?.collectionView.bounces = true
    }
    
    state = .playing
  }
  
  @IBAction func createBookmark(_ sender: Any) {
    guard let player = audioPlayer else { return }
    let currentTime = player.currentTime().seconds
    
    guard currentTime != 0, bookmarks.filter({$0.time == currentTime}).isEmpty else {return}
    let time = currentTime
    
    let bookmark = Bookmark(time: time)
    bookmarks.append(bookmark)
  }

  func updateButtons() {
    switch state {
    case .playing:
      playBtn.setImage(UIImage(named: "__ Pause Btn act"), for: .normal)
      playBtn.setImage(UIImage(named: "__ Pause Btn act"), for: .highlighted)

      playBtn.isEnabled = true
    case .paused:
      playBtn.setImage(UIImage(named: "__ Play Btn"), for: .normal)
      playBtn.setImage(UIImage(named: "__ Play Btn act"), for: .highlighted)
      playBtn.isEnabled = true
    case.downloading: break
    case .none: playBtn.isEnabled = true
    }
  }
  
  private func update(with time: CMTime) {
    guard time.seconds.isNormal else {return}
    
    if shouldObserveAudioPlayer, waveformPoints.count > 0,
      collectionView.contentOffset.x >= -collectionView.contentInset.left {
      let offset = ((time.seconds) * 70) - Double(collectionView.frame.width) / 2
      collectionView.setContentOffset(CGPoint(x: offset, y: 0), animated: false)
    }
    if let duration = audioPlayer?.currentItem?.duration.seconds {
      totalTimeLabel.text = duration.humanReadbleString
    }
    timeLabel.text = time.seconds.humanReadbleString
  }

  @IBAction func togglePlayback(_ sender: Any) {
    if state == .playing {
      audioPlayer?.pause()
      state = .paused
    } else {
      if state == .none {
        
      } else {
        audioPlayer?.play()
        state = .playing
      }
    }
  }
  
  func renderPoints(for cell: WaveformCell, at indexPath: IndexPath) {
    let startingPoint = indexPath.row * 70
    
    var cellPoints = [CGFloat]()
    
    for i in (startingPoint ... startingPoint + 70) {
      let point = waveformPoints[safe: i] ?? 0
      cellPoints.append(CGFloat(point))
    }
    cell.waveformView.points = cellPoints
    cell.waveformView.setNeedsDisplay()
  }
  
  func bookmarkPressed(_ gestureRecognizer: UITapGestureRecognizer) {
    guard let view = gestureRecognizer.view as? BookmarkView else {return}
    view.becomeFirstResponder()
    let targetRect = CGRect(x: view.frame.width / 2, y: view.tooltipTopOffset + view.tooltipHeight / 2, width: 0, height: 0)
    selectedBookmark = view.bookmark
    let menu = UIMenuController.shared
    menu.menuItems = [UIMenuItem(title: "Remove", action: #selector(removeSelectedBookmark))]
    menu.setTargetRect(targetRect, in: view)
    menu.setMenuVisible(true, animated: true)
  }
  
  func removeSelectedBookmark() {
    
    guard let bookmark = selectedBookmark else {return}
    
    var bookmarkData:[TimeInterval]  = bookmarks.map {$0.time}
    
    if let indexToRemove = bookmarkData.index(of: bookmark.time) {
      bookmarkData.remove(at: indexToRemove)
    }
    
    removeBookmark(bookmark)
  }
  
  func removeBookmark(_ bookmark: Bookmark) {
    guard let index = bookmarks.index(where: { $0 == bookmark }) else {return}
    bookmarks.remove(at: index)
    
    if let view = bookmarkViews.filter({ $0.bookmark == bookmark }).first, let index = bookmarkViews.index(of: view) {
      bookmarkViews.remove(at: index)
      view.removeFromSuperview()
    }
  }
  
  func renderBookmarks(for cell: WaveformCell, at indexPath: IndexPath) {    
    let bookmarksToRender = bookmarks.filter({$0.time > Double(indexPath.row) && $0.time < Double(indexPath.row + 1) })
    
    if !bookmarksToRender.isEmpty {
      for bookmark in bookmarksToRender {
        let offset = (CGFloat(bookmark.time) - CGFloat(indexPath.row))*70
        
        let rect = CGRect(x: offset - (bookmarkWidth / 2), y: 0, width: bookmarkWidth, height: collectionView.frame.height)
        let view = BookmarkView(frame: rect,
                                bookmark: bookmark,
                                tooltipHeight: bookmarkViewTooltipHeight,
                                tooltipTopOffset: timelineViewHeight - bookmarkViewTooltipHeight + 2,
                                lineWidth: indicatorWidth, lineColor:.info)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bookmarkPressed(_:))))
        view.layer.zPosition = 900 // add below time indicator
        bookmarkViews.append(view)
        
        
        cell.contentView.addSubview(view)
      }
    }
  }
  
  
  func timeFromScrollViewOffset() -> TimeInterval {
    let offset = timeFromOffset(collectionView.contentOffset.x + collectionView.contentInset.left)
    //print(max(offset, 0))
    return max(offset, 0)
  }
  
  func timeFromOffset(_ offset: CGFloat) -> TimeInterval {
    return Double(offset / 70)
  }
  
  func scrubTo(time: TimeInterval) {
    let time = Int64(time*70)
    audioPlayer?.seek(to: CMTimeMake(time , 70))
  }
  
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let count =  Int((Float(waveformPoints.count) / 70).rounded(.up))
    return count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! WaveformCell
    let intervalIndexPath = TimeInterval(indexPath.row)
    cell.timeLabel.text = intervalIndexPath.humanReadbleString
    renderPoints(for: cell, at: indexPath)
    renderBookmarks(for: cell, at: indexPath)
    return cell
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    guard !decelerate else  {return}
    handleUserTriggeredScrub()
  }
  
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    shouldObserveAudioPlayer = false
    if state == .playing {
      requiresPlayAfterUserInteraction = true
    }
    state = .paused
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    handleUserTriggeredScrub()
  }
  
  func handleUserTriggeredScrub() {
    scrubTo(time: timeFromScrollViewOffset() )
    shouldObserveAudioPlayer = true
    
    guard requiresPlayAfterUserInteraction else {return}
    state = .playing
    requiresPlayAfterUserInteraction = false
  }

}

