//
//  DrawWaveform.swift
//  StreamTest
//
//  Created by Jonathan Ortheden on 7/13/17.
//  Copyright © 2017 Secure Apps. All rights reserved.
//

import UIKit

class DrawWaveform: UIView {
  
  var points = [CGFloat]()

  override func draw(_ rect: CGRect) {
    
    var lineIndex = 0
    let distanceBetweenPoints: CGFloat = 1.0
    let lineWidth: CGFloat = 1.0
    let amplitudeCoefficient: CGFloat = 0.01
    
    var topPath: UIBezierPath?
    var bottomPath: UIBezierPath?
    
    topPath = UIBezierPath()
    bottomPath = UIBezierPath()
    
    topPath?.lineWidth = lineWidth
    bottomPath?.lineWidth = lineWidth
    
    //start drawing at:
    topPath?.move(to: CGPoint(x: 0.0 , y: rect.height/2))
    bottomPath?.move(to: CGPoint(x: 0.0 , y: rect.height))
    
    for _ in points{
      
      let moveBy = distanceBetweenPoints
      
      if lineIndex != 0 {
        topPath?.move(to: CGPoint(x: topPath!.currentPoint.x + moveBy , y:topPath!.currentPoint.y ))
      }
      
      let newY = topPath!.currentPoint.y - (points[lineIndex] * amplitudeCoefficient) - 1.0
      topPath?.addLine(to: CGPoint(x: topPath!.currentPoint.x  , y: newY))
      topPath?.close()
      lineIndex += 1
    }
    
    UIColor.brand.set()
    topPath?.stroke()
    topPath?.fill()
    
    lineIndex = 0
    bottomPath?.move(to: CGPoint(x:0.0 , y:rect.height/2 ))
    
    for _ in points{

      let moveBy = lineIndex == 0 ? 0 : distanceBetweenPoints

      bottomPath?.move(to: CGPoint(x:bottomPath!.currentPoint.x + moveBy , y:bottomPath!.currentPoint.y ))
      bottomPath?.addLine(to: CGPoint(x:bottomPath!.currentPoint.x  , y:bottomPath!.currentPoint.y - ((-1.0 * points[lineIndex]) * amplitudeCoefficient)))
      bottomPath?.close()
      lineIndex += 1
    }
    
    
    UIColor.brandLight.set()
    //Reflection and make it transparent
    bottomPath?.stroke(with: CGBlendMode.normal, alpha: 1.0)
    
    //If you want to fill it as well
    bottomPath?.fill()
  }

}
