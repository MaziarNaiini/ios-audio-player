//
//  WaveformCell.swift
//  StreamTest
//
//  Created by Jonathan Ortheden on 7/13/17.
//  Copyright © 2017 Secure Apps. All rights reserved.
//

import UIKit

class WaveformCell: UICollectionViewCell {
  
  @IBOutlet weak var timeLabel: UILabel!

  @IBOutlet weak var waveformView: DrawWaveform!
  
  var drawBookmarks = [Float]()
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    clipsToBounds = false
    
    for subview in contentView.subviews {
      if type(of: subview) == BookmarkView.self {
        subview.removeFromSuperview()
      }
    }
    
  }
  
}
