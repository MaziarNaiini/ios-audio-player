//
//  BookmarkView.swift
//  Auddly
//
//  Created by Jonathan Ortheden on 8/9/16.
//  Copyright © 2016 Auddly. All rights reserved.
//

import UIKit

class BookmarkView: UIView {
  
  var bookmark: Bookmark!
  var tooltipHeight: CGFloat
  var tooltipTopOffset: CGFloat
  var lineWidth: CGFloat
  var lineColor: UIColor
  
  var lineView: UIView
  var tooltipView: UIImageView
  
  init(frame: CGRect, bookmark: Bookmark, tooltipHeight: CGFloat, tooltipTopOffset: CGFloat, lineWidth: CGFloat, lineColor: UIColor) {
    self.bookmark = bookmark
    self.tooltipHeight = tooltipHeight
    self.tooltipTopOffset = tooltipTopOffset
    self.lineWidth = lineWidth
    self.lineColor = lineColor
    
    let lineTopOffset = tooltipTopOffset + tooltipHeight / 2
    lineView = UIView(frame: CGRect(x: (frame.size.width - lineWidth) / 2, y: lineTopOffset, width: lineWidth, height: frame.size.height - lineTopOffset))
    lineView.backgroundColor = lineColor
    
    let tooltipWidth = min(frame.size.width, 12)
    tooltipView = UIImageView(frame: CGRect(x: (frame.size.width - tooltipWidth) / 2, y: tooltipTopOffset, width: tooltipWidth, height: tooltipHeight))
    if lineColor == .info {
      tooltipView.image = UIImage(named: "Bookmark head blue")
    } else {
      tooltipView.image = UIImage(named: "Bookmark head")
    }
    tooltipView.contentMode = .scaleAspectFit
    
    super.init(frame: frame)
    
    addSubview(lineView)
    addSubview(tooltipView)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override var canBecomeFirstResponder: Bool {
    return true
  }
  
  // disables the default copy action
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    return false
  }
}
